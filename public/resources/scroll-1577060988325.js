(function(window, undefined) {

    /*********************** START STATIC ACCESS METHODS ************************/

    jQuery.extend(jimUtil, {
        "loadScrollBars": function() {
            jQuery(".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-4a8ed319-5de2-4461-b83c-c0c343fb07ec .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-d80d9b6c-225b-48eb-a446-fe5f897a522a .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-9b1aa84a-d23b-48d3-b31a-32980a928444 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-4b5e2bc6-12d8-43f7-bf2e-a3b1d0f99947 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
            jQuery(".s-706e5d8b-c8c2-433e-9d3d-e05be409c360 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:false, backgroundColor:'#333333', opacity:'0.7', thickness:'4'});
         }
    });

    /*********************** END STATIC ACCESS METHODS ************************/

}) (window);