(function(window, undefined) {

  var jimLinks = {
    "f85875fb-5e23-4dde-ad82-c034f3989f63" : {
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_3" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_4" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Line_2" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ],
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Button_1" : [
        "d80d9b6c-225b-48eb-a446-fe5f897a522a"
      ],
      "Line_2" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ],
      "Hotspot_1" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ]
    },
    "9d35966f-3a26-47a8-8a44-6c7e1b5154fc" : {
      "Hotspot_5" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_6" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ]
    },
    "4a8ed319-5de2-4461-b83c-c0c343fb07ec" : {
    },
    "d80d9b6c-225b-48eb-a446-fe5f897a522a" : {
      "Line_2" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ],
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "9b1aa84a-d23b-48d3-b31a-32980a928444" : {
      "Button_1" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ]
    },
    "4b5e2bc6-12d8-43f7-bf2e-a3b1d0f99947" : {
      "Button_3" : [
        "9b1aa84a-d23b-48d3-b31a-32980a928444"
      ],
      "Line_2" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Hotspot_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ]
    },
    "706e5d8b-c8c2-433e-9d3d-e05be409c360" : {
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ],
      "Button_3" : [
        "4b5e2bc6-12d8-43f7-bf2e-a3b1d0f99947"
      ],
      "Line_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ],
      "Hotspot_1" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);