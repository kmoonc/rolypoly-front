function callGetEvent1Service() {
    console.log("clickedEvent = " + jimData.variables["clickedEvent"]);
    console.log("eventImgSrc = " + jimData.variables["eventImgSrc"]);
    //선물상자이미지 이벤트와 연동
    document.getElementById('s-Image_7').firstElementChild.setAttribute("src", jimData.variables["eventImgSrc"]);
    //앞에서 선택한 이벤트 정보 읽어와서 출력
    $.ajax({
        type: "GET",
        url: "http://" + jimData.variables["rolypolyUrl"] + "/promotion/event/" + jimData.variables["clickedEvent"],
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function(data, status, xhr) {
            $("#rtr-s-Button_1_0").html(data.eventNm);
            //이벤트 상세내용 출력 = 기본출력문구 + (이미지있는 경우)이미지
            var eventParaTxt = data.notiDtlsCtt;
            if (data.imgFileNm != undefined && data.imgFileNm != null && data.imgFileNm.length > 0) {
                eventParaTxt += "<br><br><div id='eventPromotionImg'><img src='./images/";
                eventParaTxt += data.imgFileNm;
                eventParaTxt += "' width='250'></div>"; //화면영역사이즈가 250이므로 세팅
            }
            $("#rtr-s-Paragraph_1_0").html(eventParaTxt);
        },
        error: function(msg) {
            alert(msg);
        }
    });
}

jQuery("#simulation")
    .on("click", ".s-706e5d8b-c8c2-433e-9d3d-e05be409c360 .click", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Button_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/706e5d8b-c8c2-433e-9d3d-e05be409c360"
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_2")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/f85875fb-5e23-4dde-ad82-c034f3989f63",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_3")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/4b5e2bc6-12d8-43f7-bf2e-a3b1d0f99947"
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Line_2")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/f85875fb-5e23-4dde-ad82-c034f3989f63",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/f85875fb-5e23-4dde-ad82-c034f3989f63",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageload", ".s-706e5d8b-c8c2-433e-9d3d-e05be409c360 .pageload", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-eventId")) {

            callGetEvent1Service();

            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "target": ["#s-eventId"],
                            "value": {
                                "datatype": "variable",
                                "element": "eventId"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    });