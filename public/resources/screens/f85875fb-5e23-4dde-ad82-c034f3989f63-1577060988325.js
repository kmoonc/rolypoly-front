function callGetService() {
    console.log("custNo = " + jimData.variables["custNo"]);
    $.ajax({
        type: "GET",
        url: "http://" + jimData.variables["rolypolyUrl"] + "/promotion/" + jimData.variables["custNo"],
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function(data, status, xhr) {
            $("#rtr-s-Paragraph_1_0").html(data[0].eventNm);
            $("#rtr-s-Paragraph_3_0").html(data[1].eventNm);
            $("#rtr-s-Paragraph_4_0").html(data[2].eventNm);

            //스와이프패널 세개에 서버에서 내려온 이벤트 세개를 각각 세팅
            jimData.variables["eventId1"] = data[0].eventId;
            jimData.variables["eventId2"] = data[1].eventId;
            jimData.variables["eventId3"] = data[2].eventId;

            //첫번째 패널의 이벤트ID를 기본세팅
            jimData.variables["clickedEvent"] = jimData.variables["eventId1"];

            console.log("eventId1 = " + jimData.variables["eventId1"]);
            console.log("eventId2 = " + jimData.variables["eventId2"]);
            console.log("eventId3 = " + jimData.variables["eventId3"]);
        },
        error: function(msg) {
            alert(msg);
        }
    });
}

function setFinalevent() {
    if (document.getElementById('s-Panel_1').className.indexOf('hidden') < 0) {
        jimData.variables["clickedEvent"] = jimData.variables["eventId1"];
        jimData.variables["eventImgSrc"] = document.getElementById('s-Image_4').firstElementChild.getAttribute("src");
    } else if (document.getElementById('s-Panel_2').className.indexOf('hidden') < 0) {
        jimData.variables["clickedEvent"] = jimData.variables["eventId2"];
        jimData.variables["eventImgSrc"] = document.getElementById('s-Image_5').firstElementChild.getAttribute("src");
    } else if (document.getElementById('s-Panel_3').className.indexOf('hidden') < 0) {
        jimData.variables["clickedEvent"] = jimData.variables["eventId3"];
        jimData.variables["eventImgSrc"] = document.getElementById('s-Image_6').firstElementChild.getAttribute("src");
    }

    console.log("clickedEvent = " + jimData.variables["clickedEvent"]);
    console.log("eventImgSrc = " + jimData.variables["eventImgSrc"]);
}

jQuery("#simulation")
    .on("click", ".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .click", function(event, data) {

        setFinalevent(); //최종이벤트 및 선물이미지 저장(호출위치 : 클릭,좌스와이프,우스와이프)

        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Image_43")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_3"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Image_44")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_2"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/706e5d8b-c8c2-433e-9d3d-e05be409c360",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Image_46")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_3"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Image_45")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_1"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_3")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/706e5d8b-c8c2-433e-9d3d-e05be409c360",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Image_48")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_1"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Image_47")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_2"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_4")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/706e5d8b-c8c2-433e-9d3d-e05be409c360",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Line_2")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/9d35966f-3a26-47a8-8a44-6c7e1b5154fc",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724",
                            "transition": {
                                "type": "fade",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageload", ".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .pageload", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-f85875fb-5e23-4dde-ad82-c034f3989f63")) {

            callGetService(); //페이지로드시 이벤트세팅      

            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "variable": ["title1"],
                            "value": ""
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Button_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "target": ["#s-Button_1"],
                            "value": {
                                "datatype": "variable",
                                "element": "title1"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Text_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "target": ["#s-Text_1"],
                            "value": {
                                "datatype": "variable",
                                "element": "custNm"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-custNo")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "target": ["#s-custNo"],
                            "value": {
                                "datatype": "variable",
                                "element": "custNo"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageunload", ".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .pageunload", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-eventId")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "variable": ["eventId"],
                            "value": {
                                "datatype": "property",
                                "target": "#s-eventId",
                                "property": "jimGetValue"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("swipeleft", ".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .swipeleft", function(event, data) {

        setFinalevent(); //최종이벤트 및 선물이미지 저장(호출위치 : 클릭,좌스와이프,우스와이프)

        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Panel_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_2"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Panel_2")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_3"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Panel_3")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_1"],
                            "transition": {
                                "type": "slideleft",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("swiperight", ".s-f85875fb-5e23-4dde-ad82-c034f3989f63 .swiperight", function(event, data) {

        setFinalevent(); //최종이벤트 및 선물이미지 저장(호출위치 : 클릭,좌스와이프,우스와이프)

        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Panel_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_3"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Panel_2")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_1"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Panel_3")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimShow",
                        "parameter": {
                            "target": ["#s-Panel_2"],
                            "transition": {
                                "type": "slideright",
                                "duration": 700
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    });