jQuery("#simulation")
    .on("click", ".s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc .click", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();

        $("#name_input").focus();

        if (jFirer.is("#s-Hotspot_2")) {
            cases = [{
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Paragraph_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "93"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Input_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "154"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Text_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "164"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "167"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Hotspot_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "141"
                                },
                                "left": {
                                    "type": "pincenter",
                                    "value": "0"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "153"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimChangeStyle",
                            "parameter": [{
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }],
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimSetValue",
                            "parameter": {
                                "variable": ["custNo"],
                                "value": "000000002"
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_3")) {
            cases = [{
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Paragraph_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "93"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Input_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "154"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Text_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "164"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "167"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Hotspot_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "141"
                                },
                                "left": {
                                    "type": "pincenter",
                                    "value": "0"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "153"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimChangeStyle",
                            "parameter": [{
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }],
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimSetValue",
                            "parameter": {
                                "variable": ["custNo"],
                                "value": "000000003"
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_4")) {
            cases = [{
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Paragraph_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "93"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Input_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "154"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Text_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "164"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "167"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Hotspot_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "141"
                                },
                                "left": {
                                    "type": "pincenter",
                                    "value": "0"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "153"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimChangeStyle",
                            "parameter": [{
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_1": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_5": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }],
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimSetValue",
                            "parameter": {
                                "variable": ["custNo"],
                                "value": "000000004"
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_1")) {
            cases = [{
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "65"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Paragraph_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "93"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Input_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "94"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "154"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Text_1"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "164"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "167"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Hotspot_5"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "141"
                                },
                                "left": {
                                    "type": "pincenter",
                                    "value": "0"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimMove",
                            "parameter": {
                                "target": ["#s-Rectangle_6"],
                                "top": {
                                    "type": "movetoposition",
                                    "value": "153"
                                },
                                "left": {
                                    "type": "movetoposition",
                                    "value": "66"
                                },
                                "containment": false,
                                "effect": {
                                    "type": "none",
                                    "easing": "swing",
                                    "duration": 500
                                }
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimChangeStyle",
                            "parameter": [{
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_7": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_6": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8 > svg": {
                                    "attributes": {
                                        "overlay": "none"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Image_8": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_3": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_4": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes": {
                                        "opacity": "0.1"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }, {
                                "#s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc #s-Paragraph_2": {
                                    "attributes-ie8lte": {
                                        "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)",
                                        "filter": "alpha(opacity=10)"
                                    }
                                }
                            }],
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "parallel",
                    "delay": 0
                },
                {
                    "blocks": [{
                        "actions": [{
                            "action": "jimSetValue",
                            "parameter": {
                                "variable": ["custNo"],
                                "value": "000000001"
                            },
                            "exectype": "serial",
                            "delay": 0
                        }]
                    }],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_5")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724"
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Hotspot_6")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimNavigation",
                        "parameter": {
                            "target": "screens/9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageload", ".s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc .pageload", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Rectangle_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "target": ["#s-Rectangle_1"],
                            "value": null
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageunload", ".s-9d35966f-3a26-47a8-8a44-6c7e1b5154fc .pageunload", function(event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) { data = event; }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Input_1")) {
            cases = [{
                "blocks": [{
                    "actions": [{
                        "action": "jimSetValue",
                        "parameter": {
                            "variable": ["custNm"],
                            "value": {
                                "datatype": "property",
                                "target": "#s-Input_1",
                                "property": "jimGetValue"
                            }
                        },
                        "exectype": "serial",
                        "delay": 0
                    }]
                }],
                "exectype": "serial",
                "delay": 0
            }];
            event.data = data;
            jEvent.launchCases(cases);
        }
    });